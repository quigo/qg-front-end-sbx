// https://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parserOptions: {
    parser: "babel-eslint"
  },
  env: {
    browser: true,
    jest: true,
    node: true
  },
  globals: {
    expect: true
  },
  // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
  // consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
  extends: ["plugin:vue/essential", "airbnb-base"],
  // required to lint *.vue files
  plugins: ["vue"],
  // check if imports actually resolve
  settings: {
    "import/resolver": {
      webpack: {
        config: "build/webpack.config.dev.js"
      }
    }
  },
  // add your custom rules here
  rules: {
    "linebreak-style": 0,
    "no-nested-ternary": "error",
    "max-nested-callbacks": ["error", 3],
    "max-lines": ["error", {"max": 450, "skipBlankLines": true, "skipComments": true}],
    "max-depth": ["error", 5],
    "complexity": ["error", 10],
    "no-underscore-dangle": ["error", { "allow": ["_id"] }],  
    // don't require .vue extension when importing
    "import/extensions": [
      "error",
      "always",
      {
        js: "never",
        vue: "never"
      }
    ],
    "no-unused-vars": ["error", { argsIgnorePattern: "^_" }],
    "arrow-parens": ["error", "as-needed"],
    "comma-dangle": ["error", "only-multiline"],
    // disallow reassignment of function parameters
    // disallow parameter object manipulation except for specific exclusions
    "no-param-reassign": [
      "error",
      {
        props: true,
        ignorePropertyModificationsFor: [
          "state", // for vuex state
          "acc", // for reduce accumulators
          "e" // for e.returnvalue
        ]
      }
    ],
    // allow optionalDependencies
    "import/no-extraneous-dependencies": [
      "error",
      {
        optionalDependencies: ["test/unit/index.js"]
      }
    ],
    // allow debugger during development
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-underscore-dangle": "off",
  }
};
