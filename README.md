# qg-front-end-sbx

QG front-end sandbox project for recruitment purpose.

## exercise

Build secured user friendly interface to allow classroom management.

### Security

What is called secured is the fact that the user need to be authenticated to manage classroom.

### School classroom manager

- Students who have a firstname, a lastname, a grade and a classroom. The UI should allow us to add, update, list and delete student.

- Classrooms which have a name, a number of student and a maximun size. Each classroom has maximum size of 24 students :smile:. We should be able to create, update, list and delete classroom.

- Teachers who have a firstname, lastname and one or two classroom. The UI should allow us to add, update, list and delete teacher.

### Constraints

As you will notice there is linter and coverage threshold define in eslintrc and jest.conf.js files. Applicant code need to be compliant as possible to these configuration. But it should not block you to deliver your application.
For ignoring linter start by checking >>[here](https://eslint.org/docs/user-guide/configuring#eslintignore)<<

### Directories struture

We choose to go for this approach based on vue+webpack [article](https://itnext.io/vuejs-and-webpack-4-from-scratch-part-1-94c9c28a534a) :

    1. main.js : The application entrypoint.
    
    2. App.vue: The root component
    
    3. pages: A folder containing all top-level components, each of these will have a route entry associated with it.
    
    3. components: A folder containing our building block components. Components will be organised into sub-folders based on feature.
    
    4. router: A folder for all our vue-router configuration.
