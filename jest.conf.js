const path = require('path');

module.exports = {
  rootDir: path.resolve(__dirname, '.'),
  moduleFileExtensions: ['js', 'json', 'vue'],
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1',
    '\\.scss$': 'identity-obj-proxy'
  },
  transform: {
    '^.+\\.js$': '<rootDir>/node_modules/babel-jest',
    '.*\\.(vue)$': '<rootDir>/node_modules/vue-jest'
  },
  testURL: 'http://localhost/',
  snapshotSerializers: ['<rootDir>/node_modules/jest-serializer-vue'],
  setupFiles: ['<rootDir>/jest.vue.setup'],
  coverageDirectory: '<rootDir>/coverage',
  collectCoverageFrom: [
    'src/**/*.{js,vue}',
    '!src/main.js',
    '!src/plugins/**', // No need to test plugins
    '!src/router/index.js',
    '!src/**/*.routes.js', // No need to test routes
    '!**/node_modules/**'
  ],
  coveragePathIgnorePatterns: [],
  coverageReporters: ['lcov'],
  coverageThreshold: {
    global: {
      statements: 85,
      branches: 85,
      functions: 85,
      lines: 85
    }
  }
};
