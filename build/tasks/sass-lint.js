const gulp = require('gulp');
const gulpIf = require('gulp-if');
const sassLint = require('gulp-sass-lint');
const styles = ['src/**/*.scss'];

function sassLintTask(fail) {
  return gulp.src(styles)
      .pipe(sassLint())
      .pipe(sassLint.format())
      .pipe(gulpIf(fail, sassLint.failOnError()));
}

gulp.task('sass-lint', () => sassLintTask(true));
